import { Injectable } from '@angular/core';
import { UsuarioDataI } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [  ];

  constructor() { }

  getUsuario(): UsuarioDataI[]{
    //Slice retorna una copia del array
    return this.listUsuarios.slice();
    
  }

  agregarUsuario(usuario: UsuarioDataI) {
    this.listUsuarios.unshift(usuario);
    // localStorage.setItem('listUsuario',JSON.stringify(this.listUsuarios));
    // this.guardarInformacion();

    if(localStorage.getItem('guardado') === null){
      this.listUsuarios = [];
      this.listUsuarios.unshift(usuario);
      localStorage.setItem('guardado', JSON.stringify(this.listUsuarios))
    }else{
      this.listUsuarios = JSON.parse(localStorage.getItem('guardado') || '');
      this.listUsuarios.unshift(usuario);
      localStorage.setItem('guardado', JSON.stringify(this.listUsuarios))
    }
    // console.log(this.listUsuarios, 'lista de guardarMensaje');
    // this.listUsuarios.reset()
  }

  eliminarUsuario(usuario: any){
    this.listUsuarios = this.listUsuarios.filter(data => {
      return data.nombre.toString() !== usuario.toString();
    })
    localStorage.setItem('guardado', JSON.stringify(this.listUsuarios));
    // this.guardarInformacion();
  }

  buscarUsuario(id: string): UsuarioDataI{
    //o retorna un json {} vacio
    return this.listUsuarios.find(element => element.nombre === id) || {} as UsuarioDataI;
  }
    
  modificarUsuario(user: any){
    this.eliminarUsuario(user.nombres);
    this.agregarUsuario(user);
  }

  obtenerLocalStorage(){
    let listUsuarios = JSON.parse(localStorage.getItem('guardado') || '');
    console.log(listUsuarios);
    
    // this.listUsuarios = usuarioLista
    // console.log(usuarioLista, 'obtener LocalStorage');
    
  }
}
