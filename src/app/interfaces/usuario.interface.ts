export interface UsuarioI {
    usuario: string;
    password: string;
}


export interface UsuarioDataI {
    nombre: string;
    email: string;
    usuario: string;
    password1: string;
    password2: string;
}

export interface ForoI {
    foroid : number,
    usuario: string,
    seccion: number,
    tema: string,
    foro: string
}