import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { InicioComponent } from './inicio/inicio.component';
import { NavbarComponent } from './navbar/navbar.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ReportesComponent } from './reportes/reportes.component';
import { SharedModule } from '../shared/shared.module';
import { CrearUsuarioComponent } from './usuarios/crear-usuario/crear-usuario.component';
import { VerUsuarioComponent } from './usuarios/ver-usuario/ver-usuario.component';
import { RegistroComponent } from './registro/registro.component';



// import { FormsModule } from '@angular/forms';
// import { NgxEditorModule } from 'ngx-editor';




@NgModule({
  declarations: [
    DashboardComponent,
    InicioComponent,
    NavbarComponent,
    UsuariosComponent,
    ReportesComponent,
    CrearUsuarioComponent,
    VerUsuarioComponent,
    RegistroComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    // FormsModule,
    // NgxEditorModule.forRoot({
    //   locals: {
    //     // menu
    //     bold: 'Bold',
    //     italic: 'Italic',
    //     code: 'Code',
    //     blockquote: 'Blockquote',
    //     underline: 'Underline',
    //     strike: 'Strike',
    //     bullet_list: 'Bullet List',
    //     ordered_list: 'Ordered List',
    //     heading: 'Heading',
    //     h1: 'Header 1',
    //     h2: 'Header 2',
    //     h3: 'Header 3',
    //     h4: 'Header 4',
    //     h5: 'Header 5',
    //     h6: 'Header 6',
    //     align_left: 'Left Align',
    //     align_center: 'Center Align',
    //     align_right: 'Right Align',
    //     align_justify: 'Justify',
    //     text_color: 'Text Color',
    //     background_color: 'Background Color',
    //     // popups, forms, others...
    //     url: 'URL',
    //     text: 'Text',
    //     openInNewTab: 'Open in new tab',
    //     insert: 'Insert',
    //     altText: 'Alt Text',
    //     title: 'Title',
    //     remove: 'Remove',
    //    },
    // }),
  ]
})
export class DashboardModule { }
