import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {

  form! : FormGroup;

  constructor(private activateRoute: ActivatedRoute,
            private usuarioService: UsuarioService,
            private router: Router,
            private fb: FormBuilder) {
    this.activateRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
      const usuario = this.usuarioService.buscarUsuario(id);
      console.log(usuario);

      console.log(Object.keys(usuario).length);
      

      //Verifico que la longitud del objeto es cero
      if(Object.keys(usuario).length === 0){
        this.router.navigate(['/dashboard/usuarios']);
      }

      this.form = this.fb.group({
            nombre: ['', Validators.required],
            email: ['', Validators.required],
            usuario: ['', Validators.required],
            password: ['', Validators.required],
            repetir: ['', Validators.required]
      });
      this.form.patchValue({
            nombre: usuario.nombre,
            email: usuario.email,
            usuario: usuario.usuario,
            password1: usuario.password1,
            password2: usuario.password2,
      });
    });
   }

  ngOnInit(): void {
  }

  Volver(): void{
    this.router.navigate(['/dashboard/usuarios']);
  }
}
