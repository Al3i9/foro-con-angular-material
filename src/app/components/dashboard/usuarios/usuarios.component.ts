import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { UsuarioService } from 'src/app/services/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit, AfterViewInit {


  listUsuarios: UsuarioDataI[] = [];
  // displayedColumns: string[] = ['nombres', 'apellidos', 'email', 'celular',  'fecha', 'hora', 'descripcion','acciones'];
  displayedColumns: string[] = ['nombre', 'email', 'usuario',  '', ''];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this. paginator;
    this.dataSource.sort = this.sort;
  }

  cargarUsuarios() {
    this.listUsuarios = this._usuarioService.getUsuario();
    this.dataSource = new MatTableDataSource(this.listUsuarios);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

     
    let carga = localStorage.getItem('listUsuarios')
    console.log(carga);
    
  }

  constructor(private _usuarioService: UsuarioService, 
              private _snackbar: MatSnackBar,
              private router: Router) { }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  applyFilter(event: Event){
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarUsuario(usuario: string){
    const opcion = confirm('Estas seguro de eliminar el mensaje?');
    if (opcion) {
      console.log(usuario);
      
      this._usuarioService.eliminarUsuario(usuario);
      this.cargarUsuarios();


      this._snackbar.open('El mensaje fue eliminado con éxito', '', {
        duration: 1500,
        horizontalPosition: 'center',
        verticalPosition: 'bottom'
      });
    }
    
  }

  verUsuario(usuario: string){
    console.log(usuario);
    this.router.navigate(['dashboard/ver-usuario', usuario]);

    
  }

  modificarUsuario(usuario: string){
     console.log(usuario);
     this.router.navigate(['dashboard/crear-usuario', usuario]);
  }
    
}
