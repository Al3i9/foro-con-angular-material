import { Component, OnInit } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioDataI } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { ValidadoresService } from 'src/app/services/validadores.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  adicionar : boolean = true;

  formulario!: FormGroup;

  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService,
              private _usuariosService: UsuarioService) { 

    this.crearFormulario();
      // this.cargarFormulario();

      this.adicionar = false;

  }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.formulario.get('nombre')?.invalid && this.formulario.get('nombre')?.touched;
  }

  get emailNoValido(){
    return this.formulario.get('email')?.invalid && this.formulario.get('email')?.touched;
  }

  get password1NoValido(){
    return this.formulario.get('password1')?.invalid && this.formulario.get('password1')?.touched;
  }

  get password2NoValido(){
    const password1 = this.formulario.get('password1')?.value;
    const password2 = this.formulario.get('password2')?.value;

    return (password1 === password2)? false : true;
  }

  get usuarioNoValido(){
    return (this.formulario.get('usuario')?.invalid && !this.formulario.get('usuario')?.errors?.['existe']) && this.formulario.get('usuario')?.touched;
  }

  get usuarioIgualJerjer(){
    return this.formulario.get('usuario')?.errors?.['existe']
  }

  crearFormulario(): void{
    this.formulario = this.fb.group({
      nombre: ['', Validators.required],
      email: ['', Validators.required],
      //Al menos un carácter en minúscula,al menos un carácter en mayúsculas y al menos un número(sin importar el orden)  Validators.pattern('((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30})')
      //Al menos 8 caracteres de longitud, letras minusculas, letras mayúsculas, números y caracteres especiales.  Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=.*[$@$!%*?&])(?=[^A-Z]*[A-Z]).{8,30}$/)
      password1: ['', Validators.required],
      password2: ['', Validators.required],
      usuario: ['', Validators.required, this.validadores.existeUsuario]
    }
    , {
      Validators: this.validadores.passwordsIguales('contrasena1', 'contrasena2')
    } as AbstractControlOptions
    
    )
  }

  guardar(): void{
    console.log(this.formulario.value);
    this.limpiarFormulario();

    if(!this.formulario.valid){
      return;
    }
    const user : UsuarioDataI = {
      nombre: this.formulario.value.nombre,
      email: this.formulario.value.email,
      usuario: this.formulario.value.usuario,
      password1: this.formulario.value.password1,
      password2: this.formulario.value.password2
    }

    if(this.adicionar){
      localStorage.setItem('registrado', this.formulario.value);

      this._usuariosService.agregarUsuario(user);
    }

  }

  limpiarFormulario(): void{
    this.formulario.reset({
      usuario: '',
      correo: '',
      contrasena: ''
    });
  }


}
